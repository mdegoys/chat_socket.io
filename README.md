This is a simple chat using socket.io

Launch back :

`cd back && node index.js`

Launch front :

`cd front && npm start`

Test front ;

`cd front && npm run test`

Made by Julian and Matthieu with <3
Enjoy :)
