var app = require('express')(),
    server = require('http').createServer(app),
    io = require('socket.io').listen(server); 

const archived_messages = [];

io.sockets.on('connection', function(socket) {
	console.log('socket.id :', socket.id);

	socket.emit('send_archived_messages', archived_messages)
	
	socket.on('message_to_server', function(data) {
		console.log('message received');

		archived_messages.unshift(data);
		io.emit('message_to_client', data);
	}); 
});

server.listen(8080);
