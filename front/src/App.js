import React, { Component } from 'react';
import io from 'socket.io-client';
import ChatMessages from './components/ChatMessages';
import Input from './components/Input';
import './App.css';

class App extends Component {
	constructor(props){
		super(props);

		this.state = {
			users: ['Jean-Michel', 'Mary Jane'],
			activeUserIndex: 0,
			newMessage: '',
			listMessages: []
		}

		const addMessage = (data) => {
			this.setState({ listMessages: [data, ...this.state.listMessages] });
		}

		this.socket = io('localhost:8080');

		this.socket.on('message_to_client', function(data) {
			addMessage(data);
		});
	}

	handleInput = (e) => {
		this.setState({ newMessage: e.target.value });
	}

	sendMessage = (e) => {
		e.preventDefault();
		
		// commentaire pour Julian <3 : on teste s'il y a un espace blanc avec le regex
		if (/\S/.test(this.state.newMessage)) {
		
			// we store the matches for a stock symbol in an array
			let copyNewMessage = this.state.newMessage;
			const arrayMatches = copyNewMessage.match(/\$([a-z0-9]+)/gi);
			
			// for each match we check if there is a corresponding stock price 
			if (arrayMatches) {
				let i = 0;
				arrayMatches.forEach(x => {
					x = x.replace('$','');
					this.getStockPrice(x)
					.then(res => {
						i++;
						if (res && res !== 'Unknown symbol') {
							copyNewMessage = copyNewMessage.replace(x,x+'('+res+')');
						}
						
						if (i === arrayMatches.length) {
							this.socket.emit('message_to_server', {
								user: this.state.users[this.state.activeUserIndex],
								content: copyNewMessage
							});
						}
					})
					.catch(err => {
						i++;	
						console.warn('err:', err)

						if (i === arrayMatches.length) {
							this.socket.emit('message_to_server', {
								user: this.state.users[this.state.activeUserIndex],
								content: copyNewMessage
							});
						}

					})
				})
			} else {
				this.socket.emit('message_to_server', {
					user: this.state.users[this.state.activeUserIndex],
					content: copyNewMessage
				});

			}
			this.setState({ newMessage: '' });
		}
	}

	getStockPrice = (symbol) => {
		return fetch(`https://api.iextrading.com/1.0/stock/${symbol}/price`)
		.then(res => res.json())
	}

	componentWillMount() {
		// we randomly select one of the users
		const random = Math.floor(Math.random()*this.state.users.length);
		this.setState({ activeUserIndex: random });

		// we fetch the archived messages
		this.socket.on('send_archived_messages', data => {
			this.setState({ listMessages : data });
		});
	}

  render() {
    return (
      <div id="main">
				<div id="main-chatmessages">
					<ChatMessages otherUser={this.state.users[1-this.state.activeUserIndex]} listMessages={this.state.listMessages} />
				</div>
				<div id="main-input">
					<Input valueInput={this.state.newMessage} handleInput={this.handleInput} sendMessage={this.sendMessage} /> 
				</div>
			</div>
    );
  }
} 
export default App;
