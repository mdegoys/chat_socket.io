import React from 'react';
import ChatMessage from './ChatMessage';
import './ChatMessages.css';

const ChatMessages = (props) => {
	return(
		<div id="chatmessages">
			<div className="header">Chat with {props.otherUser}</div>
			<div id="Julian" >
				{props.listMessages.map((x,i) => {
					return <div key={i} className={'positionnement-'+(props.otherUser !== x.user ? 'active':'inactive')}><div className={'message ' + (props.otherUser !== x.user ? 'active':'inactive')}><ChatMessage key={i} content={x.content} /></div></div>
				})}
			</div>
		</div>
	)
}

export default ChatMessages;
