import React from 'react';
import './ChatMessages.css';

const ChatMessage = (props) => {
	return(
		(props.content) ? <div className="text">{props.content}</div> :null
	)
}

export default ChatMessage;
