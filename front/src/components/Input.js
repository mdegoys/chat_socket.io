import React from 'react';
import './Input.css';

const Input = (props) => {
	return(
		<div>
			<form onSubmit={props.sendMessage}>
				<div id="input-send">
					<div id="input"><input type="text" value={props.valueInput} onChange={props.handleInput} placeholder="Aa" /></div>
					<div id="send" onClick={props.sendMessage}>Send</div>
				</div>
			</form>
		</div>
	)
}

export default Input;
