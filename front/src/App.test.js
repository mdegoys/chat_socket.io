import { waitForReact, ReactSelector } from 'testcafe-react-selectors';

fixture `Todo App test`
	.page `http://localhost:3000/`
	.beforeEach(async () => {
			await waitForReact();
	});

test('Test header content', async t => {
	const component = ReactSelector('ChatMessages');
	const header = component.find('div div');
		
	await t.expect(header.textContent).match(/Chat with (Jean-Michel|Mary Jane)/)
});

test('Should change the value of the input', async t => {
	const component = ReactSelector('Input');
	const input = component.find('input');

	await t
		.typeText(input, 'Peter Parker')
		.expect(input.value)
		.eql('Peter Parker');
});

test('Send button should have a text of send', async t => {
	const component = ReactSelector('Input');
	const input = component.find('input');
	const sendButton = component.find('#send');

	await t
		.expect(sendButton.textContent)
		.eql('Send');
});

test('When send button clicked, last chat messages should be the text entered', async t => {
	const componentInput = ReactSelector('Input');
	const input = componentInput.find('input');
	const sendButton = componentInput.find('#send');

	const component = ReactSelector('ChatMessages');
	const header = component.find('div #Julian div');

	await t
		.typeText(input, 'Peter Parker')
		.click(sendButton)
		.expect(header.textContent).eql('Peter Parker')
});
